﻿using System;
using System.Collections.Generic;

namespace Cardapio.Models
{
    public partial class Item
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double? Value { get; set; }
        public DateTime? RowCreated { get; set; }

        public Category Category { get; set; }
    }
}