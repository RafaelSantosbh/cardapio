﻿using System.Collections.Generic;

namespace Cardapio.Models
{
    public partial class Category
    {
        public Category()
        {
            Item = new HashSet<Item>();
        }

        public int Id { get; set; }
        public string Description { get; set; }

        public ICollection<Item> Item { get; set; }
    }
}