﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Lime.Protocol;
using System.Diagnostics;
using Take.Blip.Client;
using Chatbot;

namespace Cardapio.Chatbot
{
    public class Approach : IMessageReceiver
    {
        private readonly ISender _sender;
        private readonly Settings _settings;

        public Approach(ISender sender, Settings settings)
        {
            _sender = sender;
            _settings = settings;
        }

        public async Task ReceiveAsync(Message message, CancellationToken cancellationToken)
        {
            Trace.TraceInformation($"From: {message.From} \tContent: {message.Content}");
            await _sender.SendMessageAsync("Pong!", message.From, cancellationToken);

        }
    }
}
