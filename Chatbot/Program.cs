﻿using System;
using Take.Blip.Client.Console;

namespace Chatbot
{
    class Program
    {
        static int Main(string[] args) => ConsoleRunner.RunAsync(args).GetAwaiter().GetResult();
    }
}